package fr.altereo.addon.sequence.test

import fr.altereo.addon.commons.test.CommonsTestDataLoader
import fr.altereo.addon.commons.test.TestDataLoader
import io.jmix.core.DataManager
import io.jmix.core.SaveContext
import org.springframework.stereotype.Service

@Service
class SequenceTestDataLoader(val commonsTestDataLoader: CommonsTestDataLoader) : TestDataLoader(commonsTestDataLoader) {

    override fun load(dataManager: DataManager, saveContext: SaveContext) {
        // nothing
    }
}
