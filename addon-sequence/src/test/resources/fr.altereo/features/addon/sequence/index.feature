# language: fr

@index
Fonctionnalité: Index des séquences

    Le module **Séquence** a pour objet de fournir un générateur d'identifiant en séquence.
    C'est-à-dire :
    - Une gestion des séquences
    - Les valeurs courantes de chaque séquence

    <img src='features/images/datamodel.svg' style='width:400px;'/>

    Scénario: Index des séquences

