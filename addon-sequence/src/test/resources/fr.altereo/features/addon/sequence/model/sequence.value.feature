# language: fr

@model @ignoré @sprint
Fonctionnalité: Génération d'une valeur de sequence d'identifiant

    La valeur d'une séquence d'identifiant contient la prochaine valeur de la séquence

    type SequenceIdentifiantValue extends BaseEntity {
        sequence : SequenceIdentifiant {
            label: "sequence"
            required
        }
        period: String {
            label: "période"
            required
        }
        nextValue: String {
            label: "valeur de la séquence pour la prochaine demande"
            comment: "Nombre dans la base"
            required
        }

        properties {
           label: "Valeur d'une séquence d'identifiant pour une période"
        }
    }

    À chaque demande de la valeur d'une séquence d'identification, il faut préciser le code de la séquence et la date,
    (Ex: ANC, 25 avril 2023) pour obtenir la valeur de la séquence (Ex ANC2023-1256)
    et dans la même temps (même transaction), la prochaine valeur est calculée et stockée (c-a-d dans un select for update)

    Scénario: Demander une valeur de séquence
        Etant donné un utilisateur "admin" connecté
        Et la séquence d'identification
            | code | format de sequence   | format de période | base | première valeur |
            | ANC  | ANC%{period}%{value} | yyyyMM            | 10   | 0               |
        Quand il demande la prochaine valeur pour la séquence "ANC" et la date "2023-03-15", il obtient "0"
        Quand il redemande la prochaine valeur pour la séquence "ANC" et la date "2023-03-15", il obtient "1"
        Quand il redemande la prochaine valeur pour la séquence "ANC" et la date "2023-03-15", il obtient "2"


    Plan du Scénario: Demander des valeurs de séquence
        Etant donné un utilisateur "admin" connecté
        Et les séquences d'identification
            | id | code | format de sequence     | format de période | base | format de la valeur | première valeur |
            | 1  | ANC  | ANC%{period}%{value}   | yyyyMM            | 10   |                     | 0               |
            | 2  | BAC  | BAC-%{value}#%{period} | yyyy              | 16   |                     | 1000            |
            | 3  | AAA  | AAA%{period}%{value}   | yyyyMM            | 10   | 000                 | 0               |
            | 4  | BBB  | BBB-%{value}#%{period} | yyyy              | 16   | _____               | 1000            |
        Et les valeurs de séquences d'identification
            | séquence | période | prochaine valeur |
            | 1        | 202301  | 220              |
            | 1        | 202302  | 330              |
            | 1        | 202303  | 440              |
            | 2        | 2022    | AAAAAA0          |
            | 2        | 2023    | BBBBBB0          |
        Quand il demande la prochaine valeur pour la séquence <code> à la <date>, il obtient le <résultat>
        Exemples:
            | code    | date       | résultat         |
            | ANC     | 2023-03-15 | ANC202303440     |
            | ANC     | 2023-03-15 | ANC202303441     |
            | ANC     | 2023-03-15 | ANC202303442     |
            | BAC     | 2023-03-15 | BAC-BBBBBB0#2023 |
            | BAC     | 2023-03-15 | BAC-BBBBBB1#2023 |
            | BAC     | 2023-03-15 | BAC-BBBBBB2#2023 |
            | ANC     | 2023-03-15 | ANC202303443     |
            | ANC     | 2023-02-02 | ANC202302330     |
            | ANC     | 2023-02-02 | ANC202302331     |
            | UNKNOWN | 2023-02-02 | Erreur           |
            | ANC     | 2020-01-01 | ANC2020010       |
            | BAC     | 2022-12-12 | BAC-AAAAAA0#2022 |
            | BAC     | 2023-03-15 | BAC-BBBBBB3#2023 |
            | BAC     | 2022-12-12 | AAAAAA1#2022     |
            | ANC     | 2020-01-01 | ANC2020011       |
            | BAC     | 2022-12-12 | BAC-AAAAAA2#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA3#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA4#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA5#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA6#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA7#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA8#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAA9#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAA#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAB#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAC#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAD#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAE#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAAF#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAB0#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAB1#2022 |
            | BAC     | 2022-12-12 | BAC-AAAAAB2#2022 |
            | ANC     | 2023-03-15 | ANC202303444     |
            | ANC     | 2023-03-15 | ANC202303445     |
            | ANC     | 2023-03-15 | ANC202303446     |
            | ANC     | 2023-03-15 | ANC202303447     |
            | ANC     | 2023-03-15 | ANC202303448     |
            | ANC     | 2023-03-15 | ANC202303449     |
            | ANC     | 2023-03-15 | ANC202303450     |
            | ANC     | 2023-03-15 | ANC202303451     |
            | ANC     | 2023-03-15 | ANC202303452     |
            | AAA     | 2023-03-15 | AAA202303001     |
            | BBB     | 2023-03-15 | BBB-____0-2023   |

