# language: fr

@ui
Fonctionnalité: Affichage du référentiel des séquences d'identifiant

    Scénario: Le référentiel des séquences d'identifiant est accessible dans le menu "Référentiel" (MasterData)

    Scénario: Le référentiel des séquences d'identifiant a un écran maître et un écran détail avec un filtre par défaut (code, nom)

    Scénario: Le référentiel des séquences d'identifiant a les actions: créer, modifier, supprimer, exporter et importer

    Scénario: L'écran de détail d'une séquence d'identifiant affiche la liste des valeurs associées, il est possible de les créer/modifier/supprimer

    Scénario: L'écran de détail d'une séquence d'identifiant affiche un simulateur d'exemple, avec un valeur qui peut s'incrémenter et la simulation de la séquence
