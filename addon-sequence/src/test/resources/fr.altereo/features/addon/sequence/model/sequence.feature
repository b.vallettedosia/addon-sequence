# language: fr

@model @ignoré @sprint
Fonctionnalité: Référentiel des séquences d'identifiant

    Une séquence d'identifiant est un objet qui permet de créer un identifiant unique par incrément.

    type SequenceIdentifiant extends BaseMasterData {
        sequenceFormat: String {
            label: "Format de la séquence"
            required
        }
        periodFormat: String {
            label: "Format de la période"
            comment: "Au format SimpleDateFormat"
            required

        }
        baseValue: Integer {
            label: "base de calcul du nombre de la séquence"
            comment: "décimal, hexadecimal"
            min: 2
            max: 36
            default: 10
        }
        valueFormat: String {
            label: "Format de la valeur"
            comment: "rien si pas de restriction, ou un caractère par chiffre"
            default: "00000"
        }
        firstValue: String {
            label: "Première valeur"
            default: "0"
        }

        properties {
           label: "Référentiel des séquences d'identifiant"
           nomenclature: N0005
        }
    }

    Le format de la période doit respecter SimpleDateFormat https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/text/SimpleDateFormat.html
    La base est comprise entre 2 et 36 (utiliser Integer.valueOf(s, r) et Integer().toString(r))
    Le format de la valeur est une option, si elle est vide la valeur est affichée normalement, sinon il y a padding avec le format.

    Plan du Scénario: valider une séquence
        Etant donné un utilisateur "admin" connecté
        Quand il construit une séquence sous la forme <format de sequence> <format de période> <base> <format de la valeur> <première valeur>
        Alors il obtient le <résultat>
        Exemples:
            | format de sequence           | format de période | base | format de la valeur | première valeur | résultat | explication                                                                                        |
            | ANC%{period}%{value}         | yyyyMM            | 10   |                     | 0               | ok       |                                                                                                    |
            | ANC%{period}%{value}%{autre} | yyyyMM            | 10   |                     | 0               | ko       | seuls %{period} et %{value} sont autorisés dans le format de la séquence                           |
            | ANC%{period}                 | yyyyMM            | 10   |                     | 0               | ko       | il manque %{value} dans le format de la séquence                                                   |
            | ANC%{value}                  | yyyyMM            | 10   |                     | 0               | ko       | il manque %{period} dans le format de la séquence alors que le format de la période n'est pas vide |
            | ANC%{period}%{value}         | bbbbbbb           | 10   |                     | 0               | ko       | bbbbbbb n'est pas format de période valide (voir SimpleDateFormat)                                 |
            | ANC%{period}%{value}         | yyyyMM            | 1    |                     | 0               | ko       | la base n'est pas comprise entre 2 et 36                                                           |
            | ANC%{period}%{value}         | yyyyMM            | 37   |                     | 0               | ko       | la base n'est pas comprise entre 2 et 36                                                           |
            | BAC-%{value}#%{period}       |                   | 16   |                     | 0               | ok       | %{period} est dans le format de la séquence alors que le format de la période est vide             |
            | ANC%{period}%{value}         | yyyyMM            | 10   |                     | A               | ko       | la première valeur n'est pas valide dans la base                                                   |
            | ANC%{period}%{value}         | yyyyMM            | 10   |                     | -1              | ko       | la première valeur doit être supérieure ou égale à 0                                               |
            | %{value}-BAC                 |                   | 36   |                     |                 | ok       |                                                                                                    |
            | %{value}-BAC                 |                   | 36   | 000000              |                 | ok       |                                                                                                    |
            | %{value}-BAC                 |                   | 36   | #####               |                 | ok       |                                                                                                    |
            | %{value}-BAC                 |                   | 36   | ___                 |                 | ok       |                                                                                                    |
            | %{value}-BAC                 |                   | 36   | 000#                |                 | ko       | le format de la valeur ne peut avoir deux caractères différents                                    |


    Scénario: Lister les séquences par défaut
        Etant donné un utilisateur "admin" connecté
        Alors la liste des séquences est vide

    Scénario: Vérifier l'ensemble des séquences par défaut
        Etant donné un utilisateur "admin" connecté
        Alors la vérification des séquences vide

    Scénario: Lister les séquences de test
        Etant donné un utilisateur "admin" connecté
        Et que le chargement des données de test a été fait
        Alors la liste des séquences contient
            | code | nom  | description | clé de recherche | format de sequence | format de période | base | format de la valeur | première valeur |
            | AUTO | AUTO | AUTO        |                  | %{value}           |                   | 10   |                     | 0               |

    Scénario: Vérifier l'ensemble des séquences de test
        Etant donné un utilisateur "admin" connecté
        Et que le chargement des données de test a été fait
        Alors la vérification des séquences est ok
