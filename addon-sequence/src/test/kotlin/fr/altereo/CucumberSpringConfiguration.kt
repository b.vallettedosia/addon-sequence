package fr.altereo

import fr.altereo.addon.commons.test.CommonsCucumberSpringConfiguration
import fr.altereo.addon.sequence.SeqTestConfiguration
import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.java.Scenario
import io.cucumber.spring.CucumberContextConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.TestExecutionListeners
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener


@CucumberContextConfiguration
@SpringBootTest(classes = [SeqTestConfiguration::class], webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestExecutionListeners(listeners = [DependencyInjectionTestExecutionListener::class])
class CucumberSpringConfiguration : CommonsCucumberSpringConfiguration() {

    @Before
    override fun before(scenario: Scenario) = super.before(scenario)

    @After
    override fun after(scenario: Scenario) = super.after(scenario)
}

