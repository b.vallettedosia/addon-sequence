package fr.altereo.addon.sequence

import io.jmix.core.annotation.JmixModule
import org.springframework.boot.SpringBootConfiguration
import org.springframework.boot.autoconfigure.EnableAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Import
import org.springframework.context.annotation.Primary
import org.springframework.context.annotation.PropertySource
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType

import javax.sql.DataSource

@SpringBootConfiguration
@EnableAutoConfiguration
@Import(SeqConfiguration::class)
@JmixModule(id = "fr.altereo.addon.sequence.test", dependsOn = [SeqConfiguration::class])
class SeqTestConfiguration {

    @Bean
    @Primary
    fun dataSource(): DataSource {
        return EmbeddedDatabaseBuilder()
            .generateUniqueName(true)
            .setType(EmbeddedDatabaseType.HSQL)
            .build();
    }
}
