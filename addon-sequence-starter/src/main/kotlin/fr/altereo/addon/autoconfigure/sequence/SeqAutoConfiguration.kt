package fr.altereo.addon.autoconfigure.sequence

import fr.altereo.addon.sequence.SeqConfiguration
import org.springframework.boot.autoconfigure.AutoConfiguration
import org.springframework.context.annotation.Import

@AutoConfiguration
@Import(SeqConfiguration::class)
open class SeqAutoConfiguration {

}

